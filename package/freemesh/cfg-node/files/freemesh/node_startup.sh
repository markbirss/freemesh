#!/bin/sh
#run on startup each time
first_boot() {
	echo "`date` - first_boot() start" >> /tmp/fmstartup.log;
	/freemesh/first_boot.sh;

	uci set fm.node.state="initialize";
	uci commit fm;

	echo "`date` - first_boot() end" >> /tmp/fmstartup.log;
}

initialize() {
	echo "`date` - initialize() start" >> /tmp/fmstartup.log;
	/freemesh/initialize_node.sh; #we want this to run forever until successful

        uci set fm.node.state="normal";
        uci commit fm;

	echo "`date` - initialize() end" > /tmp/freemesh_startup.log;
}

normal_boot() {
	echo "`date` - normal_boot()" > /tmp/freemesh_startup.log;
}

#kill the trigger so we control the leds
echo "none" > /sys/class/leds/mt76-phy0/trigger;
echo "none" > /sys/class/leds/zbt-we826\:green\:wifi/trigger;

if [ `uci get fm.node.state` == "firstboot" ]; then
	first_boot;
elif [ `uci get fm.node.state` == "initialize" ]; then
	initialize;
else
	normal_boot;
fi
